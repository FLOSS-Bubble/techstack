# Techstack

You will find the techstack of some FLOSS-related people in this document.

## German

### Dirk Deimeke

- [Technik](https://deimeke.link/technik)
- [Computergeschichte](https://deimeke.link/computergeschichte)

### Jörg Kastning

- [Hardware - Dinge zum Anfassen](https://www.my-it-brain.de/wordpress/hardware-dinge-zum-anfassen/)

### Christian Stankowic

- [Liste bisher verwendeter Hardware](https://cstan.io/hardware/history/)
- [Aktuell verwendete Retro-Hardware](https://cstan.io/hardware/retro/)
- [IBM- und ThinkPad-Sammlung (Blog/Podcast)](https://thinkpad-museum.de/)
- [Bisherige und aktuelles Homelab(s)](https://cstan.io/hardware/homelab/)
